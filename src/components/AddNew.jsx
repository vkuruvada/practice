import React from 'react';


export default class AddNew extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: ''
		};
	}

	nameChanged(e) {
		this.setState({
			name: e.target.value
		});
	}

	handleClick() {

		var obj = {
			name: this.state.name
		};
		this.props.addUser(obj);
		this.setState({
			name: ''
		});
	}

	render() {
		return (
				<div>
					<input onChange={this.nameChanged.bind(this)} value={this.state.name} />
					<button onClick={this.handleClick.bind(this)}>Add new User</button>
				</div>
			)
	}
}

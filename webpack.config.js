var webpack = require('webpack'),
	path = require('path'),
	HtmlWebpackPlugin = require('html-webpack-plugin');

const PRODUCTION = process.env.NODE_ENV === 'production';
const BASE_DIR = __dirname;

var plugins = [
	new HtmlWebpackPlugin({
		template: path.join(BASE_DIR, 'src', 'index-template.html') 
	})
];

if (PRODUCTION) {
	plugins.push(new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor-[hash].js'));
}

module.exports = {

	entry: {
		app: path.join(BASE_DIR, 'src', 'index.jsx'),
		vendor: ['react', 'react-dom']
	},
	output: {
		filename: PRODUCTION ? 'bundle-[chunkhash].js' : 'bundle.js',
		publicPath: '/build/',
		path: path.join(BASE_DIR, 'build')
	},
	devtool:'source-map',
	module: {
		loaders: [
			{
				test: /\.jsx?/,
				loaders: ['babel-loader'],
				exclude: /node_modules/
			}
		]
	},

	plugins: plugins
};
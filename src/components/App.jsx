import React from 'react';
import Section from './Section.jsx';
import AddNew from './AddNew.jsx';

export default class App extends React.Component {

	constructor(props) {
		super(props);
		this.state =  {
			names: [
				{name: 'praveen'},
				{name: 'velina'},
				{name: 'pandu'}
			]
		};
	}

	addUser(user) {
		this.setState({
			names: this.state.names.concat([user])
		});
	}

	render() {
		var sections = this.state.names.map((item, index) => (
			<Section key={index} name={item.name} ></Section>
		))
		return (
			<div>
				{sections}
				<AddNew addUser={this.addUser.bind(this)} />
			</div>
		)
	}
}
